#define EXPOSE_TREACHEROUS
#include "newimage/newimageall.h"
#include "utils/threading.h"
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <execution>
#include <cstdint>
#include <tuple>

#include <boost/test/unit_test.hpp>

using namespace NEWIMAGE;

template<typename T>
void check_close(T a, T b, float tol=1e-6) {
  T diff = std::abs(a - b);
  BOOST_TEST(diff < tol);
}

struct F {
  volume4D<float>    testvol;
  std::vector<float> testsums;
  int xsz = 10;
  int ysz = 10;
  int zsz = 10;
  int tsz = 10;

  void populate() {
    testsums = std::vector<float>(tsz, 0);
    int count = 0;

    for (int t = 0; t < tsz; t++) {

      float sum = 0;

      for (auto it = testvol.vbegin(); it < testvol.vend(); it++) {
        auto [x, y, z]      = (*it);
        testvol(x, y, z, t) = count;
        sum                += (count++);
      }
      testsums[t] = sum;
    }
  }

  F() {
    testvol = volume4D<float>(xsz, ysz, zsz, tsz);
    populate();
  }
};


BOOST_FIXTURE_TEST_CASE(test_read_write, F)
{

  volume4D<float> writevol(xsz, ysz, zsz, tsz);

  // copy values from readvol to write vol
  for (int t = 0; t < tsz; t++) {

    auto copy_values = [&](std::tuple<int64_t, int64_t, int64_t> xyz) {
      auto [x, y, z] = xyz;

      float val            = testvol[t].interpolate(x, y, z);
      writevol[t](x, y, z) = val;

      // operator(x, y, z, t)calls operator[t](x,y,z),
      // so this should have no effect
      writevol(x, y, z, t) = val;

      // also test extrapolation pathway
      writevol(-1, -1, -1, t) = 1234;
    };

    Utilities::parfor(8, copy_values, testvol.vbegin(), testvol.vend());
  }

  // Check that write volume has the expected contents
  for (int t = 0; t < tsz; t++) {
    for (auto it = writevol.vbegin(); it < writevol.vend(); it++) {
      auto [x, y, z] = (*it);
      check_close(writevol(x, y, z, t), testvol(x, y, z, t));
    }
  }

  for (int t = 0; t < tsz; t++) {
    check_close(testvol[t].sum(), writevol[t].sum());
  }
}


BOOST_FIXTURE_TEST_CASE(test_reinitialize, F)
{
  for (int t = 0; t < tsz; t++) {
    float sum = testvol[t].sum();
    check_close(sum, testsums[t]);
  }

  testvol.reinitialize(xsz, ysz, zsz, tsz);
  testvol = 0;

  for (int t = 0; t < tsz; t++) {
    float sum = testvol[t].sum();
    check_close(sum, 0.0f);
  }

  populate();

  for (int t = 0; t < tsz; t++) {
    float sum = testvol[t].sum();
    check_close(sum, testsums[t]);
  }
}

// check that sv cache is invalidated on
// writes when using spline interpolation
BOOST_FIXTURE_TEST_CASE(test_splinterp, F)
{
  testvol.setinterpolationmethod(spline);

  for (int t = 0; t < tsz; t++) {

    volume<float> evalvol = testvol[t];
    evalvol.setinterpolationmethod(spline);

    check_close(evalvol   .interpolate(5, 5, 5),
                testvol[t].interpolate(5, 5, 5));

    testvol(5, 5, 5, t) = 12345;
    evalvol(5, 5, 5)    = 12345;

    check_close(evalvol   .interpolate(5, 5, 5),
                testvol[t].interpolate(5, 5, 5));
  }
}

// test that changes to interp/extrap settings on a
// 4D volume are propagated to its ShadowVolume instances
BOOST_FIXTURE_TEST_CASE(test_interp_settings_delegated, F)
{
  testvol.setinterpolationmethod(nearestneighbour);
  testvol.setextrapolationmethod(mirror);
  testvol.setpadvalue(12345);
  testvol.setsplineorder(5);
  testvol.setextrapolationvalidity(false, true, false);
  for (int i = 0; i < tsz; i++) {
    BOOST_CHECK(testvol[i].getinterpolationmethod() == nearestneighbour);
    BOOST_CHECK(testvol[i].getextrapolationmethod() == mirror);
    BOOST_CHECK(testvol[i].getpadvalue() == 12345);
    BOOST_CHECK(testvol[i].getsplineorder() == 5);
    std::vector<bool> expvalidity = {false, true, false};
    BOOST_CHECK(testvol[i].getextrapolationvalidity() == expvalidity);
  }

  testvol.setinterpolationmethod(sinc);
  testvol.setextrapolationmethod(periodic);
  testvol.setpadvalue(54321);
  testvol.setsplineorder(2);
  testvol.setextrapolationvalidity(true, false, true);
  for (int i = 0; i < tsz; i++) {
    BOOST_CHECK(testvol[i].getinterpolationmethod() == sinc);
    BOOST_CHECK(testvol[i].getextrapolationmethod() == periodic);
    BOOST_CHECK(testvol[i].getpadvalue() == 54321);
    BOOST_CHECK(testvol[i].getsplineorder() == 2);
    std::vector<bool> expvalidity = {true, false, true};
    BOOST_CHECK(testvol[i].getextrapolationvalidity() == expvalidity);
  }

  // Force the ShadowVolume cache to be cleared
  testvol.setinterpolationmethod(spline);
  testvol.invalidateSplines();

  // make sure settings still the same
  for (int i = 0; i < tsz; i++) {
    BOOST_CHECK(testvol[i].getinterpolationmethod() == spline);
    BOOST_CHECK(testvol[i].getextrapolationmethod() == periodic);
    BOOST_CHECK(testvol[i].getpadvalue() == 54321);
    BOOST_CHECK(testvol[i].getsplineorder() == 2);
    std::vector<bool> expvalidity = {true, false, true};
    BOOST_CHECK(testvol[i].getextrapolationvalidity() == expvalidity);
  }

  // Attempting to change settings
  // on a SV should error
  BOOST_CHECK_THROW(
    testvol[0].setinterpolationmethod(trilinear),
    NEWMAT::Exception);
}
