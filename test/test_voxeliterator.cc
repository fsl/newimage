#include "newimage/newimageall.h"
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <execution>
#include <cstdint>
#include <tuple>

#include <boost/test/unit_test.hpp>


using namespace NEWIMAGE;


using Voxel = std::tuple<int64_t, int64_t, int64_t>;

BOOST_AUTO_TEST_CASE(begin_end_valid)
{
  // empty volume - start equals end
  volume<float> v1;
  BOOST_CHECK(v1.vbegin() == v1.vend());

  // volume  of size N - (end - start) == N
  volume<float> v2(3, 4, 5);
  BOOST_CHECK((v2.vend() - v2.vbegin()) == 60);

  Voxel expbegin({0, 0, 0});
  Voxel expend(  {2, 3, 4});
  BOOST_CHECK(*v2.vbegin()     == expbegin);
  BOOST_CHECK(*(v2.vend() - 1) == expend);
}


BOOST_AUTO_TEST_CASE(normal_loop)
{
  volume<float> vol(3, 4, 5);
  for (int z = 0; z < 5; z++) {
  for (int y = 0; y < 4; y++) {
  for (int x = 0; x < 3; x++) {
    vol(x, y, z) = x * 100 + y * 10 + z;
  }}}

  for (auto it = vol.vbegin(); it < vol.vend(); it++) {
    auto [x, y, z] = (*it);
    BOOST_CHECK(vol(x, y, z) == x * 100 + y * 10 + z);
  }
}

BOOST_AUTO_TEST_CASE(std_for_each)
{
  volume<float> vol(3, 4, 5);
  for (int z = 0; z < 5; z++) {
  for (int y = 0; y < 4; y++) {
  for (int x = 0; x < 3; x++) {
    vol(x, y, z) = x * 100 + y * 10 + z;
  }}}

  std::vector<float> result;

  auto run_task_on_voxel = [&](const Voxel& vox) {
    auto [x, y, z] = vox;
    result.push_back(vol(x, y, z) * 10);
  };

  std::for_each(vol.vbegin(), vol.vend(), run_task_on_voxel);

  int i = 0;
  for (int z = 0; z < 5; z++) {
  for (int y = 0; y < 4; y++) {
  for (int x = 0; x < 3; x++) {
    BOOST_CHECK(vol(x, y, z) * 10 == result[i]);
    i++;
  }}}
}


BOOST_AUTO_TEST_CASE(std_transform)
{
  volume<float> vol(3, 4, 5);
  for (int z = 0; z < 5; z++) {
  for (int y = 0; y < 4; y++) {
  for (int x = 0; x < 3; x++) {
    vol(x, y, z) = x * 100 + y * 10 + z;
  }}}

  auto run_task_on_voxel = [&](const Voxel& vox) -> float{
    auto [x, y, z] = vox;
    return vol(x, y, z) * 10;
  };

  std::vector<float> result(vol.nvoxels());
  std::transform(vol.vbegin(),
                 vol.vend(),
                 result.begin(),
                 run_task_on_voxel);

  int i = 0;
  for (int z = 0; z < 5; z++) {
  for (int y = 0; y < 4; y++) {
  for (int x = 0; x < 3; x++) {
    BOOST_CHECK(vol(x, y, z) * 10 == result[i]);
    i++;
  }}}
}
